Module usage
------------------
 The price of a product might change in different currencies by a value which
 is not determined by it's actual rate. For example 1USD might be 100JPN in
 one product, but 150JPN in another. By using CCK fields it is possible to
 apply custom logic on the product's price and currency.
The module alters the price according to the user's currency.
Using CCK computed module you can have even more complex logic to determine the
price. Furthermore, as the currency settings are handled via CCK, it is possible
to export and import it from site to site by using Content copy module.

Note that this module depends on CCK 2.5 or higher. 


Module installation example
-------------------
1) Enable module and module dependencies.
2) Make sure he price handler is enabled in admin/store/settings/price-handlers
   and disable "Cache generated price values until the next cron run."
3) Add a CCK field for example in admin/content/node-type/product/fields
   Label is YEN. In this example the field name is field_uc_mc_yen.
4) In the widget settings under 'Multiple currency field settings' enable
   'Use field as currency field', Enter the ¥ sign in 'Currency sign'. and set 
   the 'Currency code' to JPY.
5) Create a product with the price of 10$, and in the new YEN CCK field enter
   20.
6) Set the user currency by going to uc_cck_currency/set/field_uc_mc_yen
7) Add product to cart - notice how the price and currency changed.
8) Reset the user's currency by going to  uc_cck_currency/reset
