<?php

/**
 * @file
 * Show different prices according to the user's currency.
 *
 * The price of a product might change in different currencies by a value which
 * is not determined by it's actual rate. For example 1USD might be 100JPN in
 * one product, but 150JPN in another. By using CCK fields it is possible to
 * apply custom logic on the product's price and currency.
 */

/**
 * Define a string that marks currency was not set.
 */
define ('UC_CCK_CURRENCY_NOT_SET', 'not set');

/*******************************************************************************
 * Hook implementations
 ******************************************************************************/

/**
 * Implementation of hook_menu().
 */
function uc_cck_currency_menu() {
  $items = array();

  $items['uc_cck_currency/set/%'] = array(
    'title' => 'Currency select',
    'access arguments' => array('access content'),
    'page callback' => 'uc_cck_currency_set_user_currency',
    'page arguments' => array(2),
    'type' => MENU_CALLBACK,
  );

  $items['uc_cck_currency/reset'] = array(
    'title' => 'Currency reset',
    'access arguments' => array('access content'),
    'page callback' => 'uc_cck_currency_reset_user_currency',
    'type' => MENU_CALLBACK,
  );
  return $items;
}

/**
 * Implementation of hook_widget_settings_alter().
 */
function uc_cck_currency_widget_settings_alter(&$settings, $op, $widget) {
  $widget_types = array('number', 'computed');
  if ((!empty($widget['type']) && in_array($widget['type'], $widget_types)) || (!empty($widget['widget_type']) && in_array($widget['widget_type'], $widget_types))) {
    switch ($op) {
      case 'form':
        $settings['uc_mc'] = array(
          '#type' => 'fieldset',
          '#title' => t('Multiple currency field settings'),
          '#collapsible' => TRUE,
          '#collapsed' => empty($widget['uc_mc_enable']),
          '#weight' => 5,
        );
        $settings['uc_mc']['uc_mc_enable'] = array(
          '#type' => 'checkbox',
          '#title' => t('Use field as currency field'),
          '#options' => array(0 => t('Disabled'), 1 => t('Enabled')),
          '#default_value' => !empty($widget['uc_mc_enable']),
        );
        $settings['uc_mc']['uc_mc_sign'] = array(
          '#type' => 'textfield',
          '#title' => t('Currency sign'),
          '#default_value' => !empty($widget['uc_mc_sign']) ? $widget['uc_mc_sign'] : '',
          '#description' => t('Enter the sign of the currency (e.g. $ for USD currency).'),
        );
        $settings['uc_mc']['uc_mc_code'] = array(
          '#type' => 'textfield',
          '#title' => t('Currency code'),
          '#default_value' => !empty($widget['uc_mc_code']) ? $widget['uc_mc_code'] : '',
          '#description' => t('Enter the 3 letter currency code (e.g. JPY for Yen).'),
        );
        break;
      case 'save':
        $settings[] = 'uc_mc_enable';
        if (!empty($widget['uc_mc_enable'])) {
          $settings[] = 'uc_mc_sign';
          $settings[] = 'uc_mc_code';
        }
        break;
    }
  }
}

/**
 * Implementation of hook_uc_price_handler().
 */
function uc_cck_currency_uc_price_handler() {
  return array(
    'alter' => array(
      'title' => t('CCK currency price handler'),
      'description' => t('Alter the price according to the user currency and the price in the CCK field.'),
      'callback' => 'uc_cck_currency_price_handler_alter',
    ),
  );
}

/**
 * Implementation of hook_order().
 *
 * Add the currency data to the order, as an 'Admin comment'.
 */
function uc_cck_currency_order($op, &$arg1, $arg2) {
  switch ($op) {
    case 'save':
      // Add the currency code of the order.
      if (!isset($arg1->data['uc_cck_currency'])) {
        if (($field_name = uc_cck_currency_get_user_currency($account)) && ($field = content_fields($field_name))) {
          $currency = _uc_cck_currency_get_widget_to_order($field);
          $arg1->data['uc_cck_currency'] = $currency;

          // Change the currency code of the order.
          $arg1->currency_code = $currency['code'];

          // Change the price of every product.
          foreach ($arg1->products as &$product) {
            $price_info = array(
              'price' => $product->price,
              'qty' => !empty($product->qty) ? $product->qty : 1,
            );
            $context['revision'] = 'altered';
            $context['subject'] = array(
              'order' => $arg1,
              'product' => $product,
              'node' => node_load($product->nid),
            );

            $options = array(
              'sign' => FALSE,
            );

            $product->price = uc_price($price_info, $context);
          }
        }
        else{
          // Mark the currency code as not set.
          $arg1->data['uc_cck_currency'] = array();
        }
        // Re-save order.
        uc_order_save($arg1);
      }

      break;
    case 'load':
      // Change the currency code of the order.
      if (empty($arg1->data['uc_cck_currency'])) {
        if (($field_name = uc_cck_currency_get_user_currency($account)) && ($field = content_fields($field_name))) {
          $arg1->data['uc_cck_currency'] = _uc_cck_currency_get_widget_to_order($field);
        }
      }
      break;
  }
}


/**
 * Price handler alterer; Change the price according to the user's currency.
 *
 * Non-default price is taken from the product CCK fields. If field isn't found
 * then the product's original price is used.
 */
function uc_cck_currency_price_handler_alter(&$price, &$context, &$options) {
  if (!empty($context['subject']['order']->data['uc_cck_currency'])) {
    // An order that exists.
    if (!empty($options['sign'])) {
      $options['sign'] = $context['subject']['order']->data['uc_cck_currency']['sign'];
    }
  }
  if ($field_name = uc_cck_currency_get_user_currency($context['account'])) {
    // Get the currency sign from the field definition.
    $field = content_fields($field_name);

    // Don't alter the sign if it should be turned off, or if there is no value
    // in the field.
    if (!empty($field['widget']['uc_mc_sign']) && !empty($options['sign'])) {
      $options['sign'] = $field['widget']['uc_mc_sign'];
    }

    if (!empty($context['subject']['node'])) {
      $node = $context['subject']['node'];
    }
    elseif (!empty($context['extras']['node'])) {
      $node = $context['extras']['node'];
    }

    if (!empty($node->{$field_name}[0]['value'])) {
      $price['price'] = $node->{$field_name}[0]['value'];
    }
  }
}

/**
 * Get the currency used in an order.
 *
 * @param $order_id
 *   Order ID.
 * @return
 *   The currency field if found, or an empty string.
 */
function uc_cck_currency_get_currency_from_order($order_id = 0) {
  $currency = '';

  return $currency;
}

/**
 * Get the currency user currency. Default currency to set if non was found.
 *
 * @param $account
 *  Optional; The user object. If empty the current user will be used.
 * @return
 *   The CCK field name of the user currency, or empty string if user has no
 *   currency.
 */
function uc_cck_currency_get_user_currency($account = NULL) {
  $currency_field = '';
  if (empty($account)) {
    global $user;
    $account = $user;
  }

  if ($account->uid && !empty($account->uc_cck_currency)) {
    $currency_field = $account->uc_cck_currency;
  }
  elseif (!empty($_SESSION['uc_cck_currency'])) {
    $currency_field = $_SESSION['uc_cck_currency'];
  }

  return $currency_field;
}

/**
 * Menu callback; Reset the user's currency.
 *
 * @param $account
 *  Optional; The user object. If empty the current user will be used.
 * @param $redirect
 *  Optional; Set to TRUE if user should be redirected after setttings the
 *  currency.
 */
function uc_cck_currency_reset_user_currency($account = NULL, $redirect = TRUE) {
  if (empty($account)) {
    global $user;
    $account = $user;
  }
  uc_cck_currency_set_user_currency('', $account, $redirect);
}

/**
 * Set the currency of a user.
 *
 * @param $currency_field
 *  Default currency to set.
 * @param $account
 *  Optional; The user object. If empty the current user will be used.
 * @param $redirect
 *  Optional; Set to TRUE if user should be redirected after setttings the
 *  currency.
 */
function uc_cck_currency_set_user_currency($currency_field = '', $account = NULL, $redirect = TRUE) {
  if (empty($account)) {
    global $user;
    $account = $user;
  }

  if ($account->uid) {
    // Authenticated user, and field name might be valid.
    user_save($account, array('uc_cck_currency' => $currency_field));
  }
  else {
    // Anonymous user. If there is no currency field then unset the session
    // value.
    if (!empty($currency_field)) {
     $_SESSION['uc_cck_currency'] = $currency_field;
    }
    else {
      unset($_SESSION['uc_cck_currency']);
    }
  }

  if ($redirect) {
    $dest = !empty($_GET['destination']) ? $_GET['destination'] : '';
    drupal_goto($dest);
  }
}

/**
 * Return fields that should be saved with the order.
 *
 * @param $field
 *   The field object.
 * @return
 *   Array with sign, code and label of the currency.
 */
function _uc_cck_currency_get_widget_to_order($field) {
  $widget = $field['widget'];
  return array(
    'sign' => check_plain($widget['uc_mc_sign']),
    'code' => check_plain($widget['uc_mc_code']),
    'label' => check_plain($widget['label']),
  );
}